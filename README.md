
## Войти в систему без пароля

Для запуска виртуальной машины использовался VirtualBox и образ CentOS-7-x86_64-Minimal-2003.iso

### Способ 1

В grub в строке linux16 `ro` заменил на `rw` и добавил `init=/bin/sh`

![](./screenshots/00-bin-sh.png)
![](./screenshots/01-bin-sh.png)


### Способ 2

rb.break

![](./screenshots/02-rd-break.png)

### Способ 3

rw init=/sysroot/bin/sh

![](./screenshots/03-sysroot.png)

### Переименование Volume Group

[script](./share/rename-volume-group)

### Добавление модуля

Процесс: [script](./share/module)

Результат:

![](./screenshots/04-tux.png)
